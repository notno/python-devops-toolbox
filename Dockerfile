FROM python:slim

WORKDIR /devops
COPY devops_toolbox devops_toolbox
COPY pyproject.toml .
RUN pip install .
RUN apt update && apt install git -y
